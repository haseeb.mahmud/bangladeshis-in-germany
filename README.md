# Bangladeshis in Germany

Number of first generation Bangladeshis living in Germany

# What's the minimum total number

As of 31st December, the minimum number of first generation Bangladeshi immigrants living in Germany is about 21000. The first generation Bangladeshis who took the German citizenship between 1971 to 1999 are not included in this count. 
